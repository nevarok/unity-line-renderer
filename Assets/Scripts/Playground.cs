﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Playground : MonoBehaviour
{
    private LineRenderer LineRenderer;

    private void Start()
    {
        LineRenderer = GetComponent<LineRenderer>();
        LineRenderer.Color = Color.white;
    }

    private void Update()
    {
        LineRenderer.LDraw(Vector3.zero, Vector3.right);
        LineRenderer.LDraw(Vector3.zero, Vector3.up);
        LineRenderer.LDraw(Vector3.zero, Vector3.left);
        LineRenderer.LDraw(Vector3.zero, Vector3.down);

        LineRenderer.LDraw(new Vector3[] { Vector3.right, Vector3.up, Vector3.left, Vector3.down, Vector3.right });
    }
}
