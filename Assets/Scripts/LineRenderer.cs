﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System;

public class LineRenderer : MonoBehaviour
{
    public Color Color { get; set; }
    public Material Material { get; set; }

    private List<IDrawable> commands;
    private event Action onGuiEvent;

    private static Material GetDefaultMaterial()
    {
        Shader shader = Shader.Find("Hidden/Internal-Colored");
        Material material = new Material(shader);

        material.hideFlags = HideFlags.HideAndDontSave;

        material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        material.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
        material.SetInt("_ZWrite", 0);

        return material;
    }

    private void Awake()
    {
        StartCoroutine(AfterRendering(Flush));
        commands = new List<IDrawable>();
        Material = Material ? Material : GetDefaultMaterial();
    }

    private IEnumerator AfterRendering(Action action)
    {
        var waitForEndOfFrame = new WaitForEndOfFrame();

        while(true)
        {
            yield return waitForEndOfFrame;

            action.Invoke();
        }
    }

    private void OnRenderObject()
    {
        Material.SetPass(0);

        foreach(var cmd in commands)
        {
            cmd.Draw();
        }
    }

    public void Flush()
    {
        commands.Clear();
    }

    public void LDraw(Vector3 start, Vector3 end)
    {
        LDraw(start, end, Color);
    }

    public void WDraw(Vector3 start, Vector3 end)
    {
        WDraw(start, end, Color);
    }

    public void LDraw(Vector3 start, Vector3 end, Color color)
    {
        Draw(start, end, color, transform.localToWorldMatrix);
    }

    public void WDraw(Vector3 start, Vector3 end, Color color)
    {
        Draw(start, end, color, Matrix4x4.identity);
    }

    public void LDraw(Vector3[] vertices)
    {
        LDraw(vertices, Color);
    }

    public void WDraw(Vector3[] vertices)
    {
        WDraw(vertices, Color);
    }

    public void LDraw(Vector3[] vertices, Color color)
    {
        Draw(vertices, color, transform.localToWorldMatrix);
    }

    public void WDraw(Vector3[] vertices, Color color)
    {
        Draw(vertices, color, Matrix4x4.identity);
    }

    public void LDraw(Vector3 center, float radius)
    {
        LDraw(center, radius, 32);
    }

    public void WDraw(Vector3 center, float radius)
    {
        WDraw(center, radius, 32);
    }

    public void LDraw(Vector3 center, float radius, int segments)
    {
        LDraw(center, radius, segments, Color);
    }

    public void WDraw(Vector3 center, float radius, int segments)
    {
        WDraw(center, radius, segments, Color);
    }

    public void LDraw(Vector3 center, float radius, int segments, Color color)
    {
        Draw(center, radius, segments, color, transform.localToWorldMatrix);
    }

    public void WDraw(Vector3 center, float radius, int segments, Color color)
    {
        Draw(center, radius, segments, color, Matrix4x4.identity);
    }

    public void Draw(Vector3 center, float radius, int segments, Color color, Matrix4x4 matrix)
    {
        float segment = 1.0f / segments;
        float deltaAngle = Mathf.PI * 2.0f * segment;
        float angle = 0.0f;

        Vector3[] vertices = new Vector3[segments + 1];

        for(int i = 0; i <= segments; i++)
        {
            float x = Mathf.Cos(angle);
            float y = Mathf.Sin(angle);

            Vector3 vector = new Vector3(x, y, 0.0f);
            vector *= radius;
            vector += center;

            vertices[i] = vector;

            angle += deltaAngle;
        }

        Draw(vertices, color, matrix);
    }

    public void Draw(Vector3 start, Vector3 end, Color color, Matrix4x4 matrix)
    {
        Line line;
        line.Start = start;
        line.End = end;
        line.Color = color;
        line.Matrix = matrix;

        commands.Add(line);
    }

    public void Draw(Vector3[] vertices, Color color, Matrix4x4 matrix)
    {
        Strip strip;
        strip.Vertices = vertices;
        strip.Color = color;
        strip.Matrix = matrix;

        commands.Add(strip);
    }

    public void LDraw(Vector3 position, string text)
    {
        LDraw(position, text, Color);
    }

    public void LDraw(Vector3 position, string text, Color color)
    {
        Vector3 worldPosition = transform.TransformPoint(position);
        WDraw(worldPosition, text, color);
    }

    public void WDraw(Vector3 position, string text)
    {
        WDraw(position, text, Color);
    }

    public void WDraw(Vector3 position, string text, Color color)
    {
        GUIStyle style = new GUIStyle();
        style.normal.textColor = color;
        style.fontSize = 12;

        Vector3 toScreen = Camera.main.WorldToScreenPoint(position);
        onGuiEvent += () => GUI.Label(new Rect(toScreen.x, Screen.height - toScreen.y, 1024, 64), text, style);
    }

    public void LRuler(Vector3 start, Vector3 end)
    {
        LRuler(start, end, 0.0f, Color);
    }

    public void LRuler(Vector3 start, Vector3 end, Color color)
    {
        LRuler(start, end, 0.0f, color);
    }

    public void LRuler(Vector3 start, Vector3 end, float offset)
    {
        LRuler(start, end, offset, Color);
    }

    public void LRuler(Vector3 start, Vector3 end, float offset, Color color)
    {
        Vector3 startWorld = transform.TransformPoint(start);
        Vector3 endWorld = transform.TransformPoint(end);

        WRuler(startWorld, endWorld, offset, color);
    }

    public void WRuler(Vector3 start, Vector3 end)
    {
        WRuler(start, end, 0.0f, Color);
    }

    public void WRuler(Vector3 start, Vector3 end, Color color)
    {
        WRuler(start, end, 0.0f, color);
    }

    public void WRuler(Vector3 start, Vector3 end, float offset)
    {
        WRuler(start, end, offset, Color);
    }

    public void WRuler(Vector3 start, Vector3 end, float offset, Color color)
    {
        Vector3 direction = end - start;
        Quaternion rotation = Quaternion.AngleAxis(90.0f, Vector3.forward);
        Vector3 offsetVector = rotation * (Vector2)direction;
        offsetVector = offsetVector.normalized * offset;

        Vector3 textPosition = direction * 0.5f + start;

        float distance = direction.magnitude;
        string text = string.Format("{0:0.00}", distance);

        start += offsetVector;
        end += offsetVector;
        textPosition += offsetVector;

        WDraw(start, end, color);
        WDraw(textPosition, text, color);
        WDraw(start, 0.05f, 4, color);
        WDraw(end, 0.05f, 4, color);
    }

    private void OnGUI()
    {
        if(Event.current.type != EventType.Repaint)
        {
            return;
        }

        if(onGuiEvent != null)
        {
            onGuiEvent();
            onGuiEvent -= onGuiEvent;
        }
    }

    private interface IDrawable
    {
        void Draw();
    }

    private struct Line : IDrawable
    {
        public Vector3 Start;
        public Vector3 End;
        public Color Color;
        public Matrix4x4 Matrix;

        public void Draw()
        {
            GL.PushMatrix();
            GL.MultMatrix(Matrix);

            GL.Begin(GL.LINES);

            GL.Color(Color);
            GL.Vertex(Start);
            GL.Vertex(End);

            GL.End();

            GL.PopMatrix();
        }
    }

    private struct Strip : IDrawable
    {
        public Vector3[] Vertices;
        public Color Color;
        public Matrix4x4 Matrix;

        public void Draw()
        {
            GL.PushMatrix();
            GL.MultMatrix(Matrix);

            GL.Begin(GL.LINE_STRIP);

            GL.Color(Color);

            foreach(var vertex in Vertices)
            {
                GL.Vertex(vertex);
            }

            GL.End();

            GL.PopMatrix();
        }
    }
}
